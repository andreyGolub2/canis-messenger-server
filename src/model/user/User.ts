export class User {
    public _id: number;
    public name: string;
    public token: string;
    public status: number;
}