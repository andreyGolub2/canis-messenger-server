export class UserInfo {
    public name: string;
    public id: number;
    public token: string;
}