import { SocketProxy } from "./SocketProxy";
import io from "socket.io";
import * as services from "../../services";

export const init = (io: io.Server) => {
    new SocketProxy(io, services.messageService());
};