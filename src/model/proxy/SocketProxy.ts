import io from "socket.io";
import { SocketMessage } from "../../const/SocketMessage";
import { AddMessageRequest } from "../conversation/AddMessageRequest";
import { Message } from "../conversation/Message";
import { TimeHelper } from "../../utils/TimeHelper";
import { ConversationInfoConverter } from "../../utils/converters/ConversationInfoConverter";
import { ServerError } from "../../errors/ServerError";
import { ServerCodes } from "../../enums/ServerCodes";
import { MessageService } from "../../services/MessageService";
import { RandomUtils } from "../../utils/RandomUtils";

export class SocketProxy {
    private readonly SOCKET_PORT: number = 8000;

    public constructor(
        private readonly socket: io.Server,
        private readonly messageService: MessageService,
    ) {
        this.init();
    }

    private init() {
        this.socket.on("connection", this.onSocketConnected.bind(this));
        
        this.socket.listen(this.SOCKET_PORT);
    }

    private onSocketConnected(socket: io.Socket) {
        console.log("New socket connected. Id: " + socket.id);

        socket.on(SocketMessage.ADD_NEW_MESSAGE, this.onAddNewMessage.bind(this));

        socket.on("disconnect", () => {
            console.log("Socket has unconnected. Id: " + socket.id);
        });
    }

    private async onAddNewMessage(messageRequest: AddMessageRequest) {
        console.log("Received new message request", messageRequest);
        if (messageRequest == null) {
            throw new ServerError("Message cannot be null", ServerCodes.SERVER_ERROR);
        }
        const message: Message = {
            id: RandomUtils.getRandomToken(),
            authorName: messageRequest.authorName,
            authorId: messageRequest.authorId,
            text: messageRequest.text,
            timestamp: TimeHelper.currentTime(),
            convId: messageRequest.conversationId,
        };
        await this.messageService.addMessage(messageRequest.conversationId, message);

        this.socket.emit(SocketMessage.RECEIVE_NEW_MESSAGE, ConversationInfoConverter.convertMessageToInfo(message));
    }

}