import { Long } from "mongodb";

export class UserDAO {
    public _id: Long;
    public name: string;
    public token: string;
    public status: Long;
}