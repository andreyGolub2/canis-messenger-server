import { Long } from "mongodb";

export class MessageDAO {
    public _id: string;
    public authorName: string;
    public authorId: Long;
    public timestamp: Long;
    public text: string;
    public convId: string;
}