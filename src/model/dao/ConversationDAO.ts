import { MessageDAO } from "./MessageDAO";

export class ConversationDAO {
    public _id: string;
    public name: string;
    public categoryId: string;
    public messages: MessageDAO[] = [];
}