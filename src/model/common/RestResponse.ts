import { ServerCodes } from "../../enums/ServerCodes";

export class RestResponse {

    public constructor(
        readonly data: any = null,
        readonly service: {
            code: number;
            message?: string
        }
    ) {}

    public static ok(data: any = null): RestResponse {
        return new RestResponse(data, { code: ServerCodes.OK });
    }

    public static fail(code: number = ServerCodes.SERVER_ERROR, message?: string): RestResponse {
        return new RestResponse(null, { code: code, message: message });
    }
}
