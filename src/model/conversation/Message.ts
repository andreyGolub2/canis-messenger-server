export class Message {
    public id: string;
    public authorName: string;
    public authorId: number;
    public timestamp: number;
    public text: string;
    public convId: string;
}