export class MessageInfo {
    public id: string;
    public authorName: string;
    public timestamp: number;
    public text: string;
    public convId: string;
}