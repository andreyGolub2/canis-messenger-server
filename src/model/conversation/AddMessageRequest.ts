export class AddMessageRequest {
    public conversationId: string;
    public authorId: number;
    public authorName: string;
    public text: string;
} 