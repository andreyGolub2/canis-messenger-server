import { MessageInfo } from "./MessageInfo";

export class ConversationInfo {
    public id: string;
    public name: string;
    public categoryId: string;
    public messages: MessageInfo[] = [];
}