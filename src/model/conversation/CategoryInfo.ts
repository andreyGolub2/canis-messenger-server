import { ConversationInfo } from "./ConversationInfo";

export class CategoryInfo {
    public id: string;
    public name: string;
    public conversations: ConversationInfo[];
}