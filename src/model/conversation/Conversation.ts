import { Message } from "./Message";

export class Conversation {
    public _id: string;
    public name: string;
    public categoryId: string;
    public messages: Message[] = [];
}