import { Router } from "express-serve-static-core";
import { MessageFacade } from "../facades/MessageFacade";

export class MessageRouter {

    public constructor(
        private router: Router,
        private facade: MessageFacade,
    ) {
        this.setup();
    }

    private setup() {
        this.router.route("/api/categories")
            .get(this.facade.getConversations.bind(this.facade));
        this.router.route("/api/createConversation")
            .post(this.facade.createConversation.bind(this.facade));
        this.router.route("/api/createCategory")
            .post(this.facade.createCategory.bind(this.facade));
        this.router.route("/api/getMessages")
            .get(this.facade.getMessagesByConv.bind(this.facade));
    }
}