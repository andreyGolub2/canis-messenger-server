import { Router } from "express-serve-static-core";
import { UserFacade } from "../facades/UserFacade";

export class UserRouter {

    public constructor(
        private router: Router,
        private facade: UserFacade,
    ) {
        this.setup();
    }

    private setup() {
        this.router.route("/api/login")
            .post(this.facade.login.bind(this.facade));
    }
}