import { Router } from "express";
import { MessageRouter } from "./MessageRouter";
import * as facades from "../facades";
import { UserRouter } from "./UserRouter";

export const init = (router: Router): void => {
    new MessageRouter(router, facades.messageFacade());
    new UserRouter(router, facades.userFacade());
};