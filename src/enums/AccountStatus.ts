export enum AccountStatus {
    ACTIVE = 0,
    ADMIN = 1,
    BANNED = 2,
}