import { ServerCodes } from "../enums/ServerCodes";

export class ServerError extends Error {

    code: number;

    constructor(message: string, code: number = ServerCodes.SERVER_ERROR) {
        super(message);
        this.code = code;
        Object.setPrototypeOf(this, ServerError.prototype);
    }

}
