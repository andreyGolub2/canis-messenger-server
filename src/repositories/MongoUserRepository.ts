import { Collection } from "mongodb";
import { MongoProxy } from "../libs/MongoProxy";
import { UserDAO } from "../model/dao/UserDAO";

const COLLECTION: string = "users";

export class MongoUserRepository {

    private collection: Collection;

    public constructor(mongoProxy: MongoProxy) {
        this.collection = mongoProxy.getState().db.collection(COLLECTION);
    }

    public async getUserByToken(token: string): Promise<UserDAO> {
        return await this.collection.findOne({ token: token });
    }

    public async update(dao: UserDAO): Promise<void> {
        await this.collection.updateOne({ _id: dao._id }, dao);
    }

    public async insert(dao: UserDAO): Promise<void> {
        await this.collection.insertOne(dao);
    }

}