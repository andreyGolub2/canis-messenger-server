import { MongoConversationRepository } from "./MongoConversationRepository";
import { mongoProxy } from "../libs";
import { MongoUserRepository } from "./MongoUserRepository";
import { MongoCategoryRepository } from "./MongoCategoryRepository";

let _conversationsRepository: MongoConversationRepository,
    _userRepository: MongoUserRepository,
    _categoryRepository: MongoCategoryRepository;

export const conversationRepository = (): MongoConversationRepository => {
    return _conversationsRepository || (_conversationsRepository = new MongoConversationRepository(mongoProxy()));
};

export const userRepository = (): MongoUserRepository => {
    return _userRepository || (_userRepository = new MongoUserRepository(mongoProxy()));
};

export const categoryRepository = (): MongoCategoryRepository => {
    return _categoryRepository || (_categoryRepository = new MongoCategoryRepository(mongoProxy()));
};