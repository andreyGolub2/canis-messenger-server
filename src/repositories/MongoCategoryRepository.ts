import { Collection } from "mongodb";
import { MongoProxy } from "../libs/MongoProxy";
import { CategoryDAO } from "../model/dao/CategoryDAO";

const COLLECTION: string = "categories";

export class MongoCategoryRepository {

    private collection: Collection;

    public constructor(mongoProxy: MongoProxy) {
        this.collection = mongoProxy.getState().db.collection(COLLECTION);
    }

    public async getAll(): Promise<CategoryDAO[]> {
        const categories: CategoryDAO[] =  await this.collection.find({}).toArray();
        return categories;
    }

    public async getByName(name: string): Promise<CategoryDAO> {
        return await this.collection.findOne({ name: name});
    }

    public async insert(dao: CategoryDAO): Promise<void> {
        await this.collection.insertOne(dao);
    } 

}