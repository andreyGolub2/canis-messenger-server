import { Collection } from "mongodb";
import { MongoProxy } from "../libs/MongoProxy";
import { ConversationDAO } from "../model/dao/ConversationDAO";
import { Message } from "../model/conversation/Message";
import { ConversationConverter } from "../utils/converters/ConversationConverter";

const COLLECTION: string = "conversations";

export class MongoConversationRepository {

    private collection: Collection;

    public constructor(mongoProxy: MongoProxy) {
        this.collection = mongoProxy.getState().db.collection(COLLECTION);
    }

    public async getAll(): Promise<ConversationDAO[]> {
        return await this.collection.find({ }).toArray();
    }

    public async get(id: string): Promise<ConversationDAO> {
        return await this.collection.findOne({ _id: id });
    }

    public async update(dao: ConversationDAO): Promise<void> {
        await this.collection.updateOne({ _id: dao._id }, { $set : dao });
    }

    public async insert(dao: ConversationDAO): Promise<void> {
        await this.collection.insertOne(dao);
    }

    public async addMessage(message: Message): Promise<void> {
        const dao: ConversationDAO = await this.get(message.convId);
        const messageDAO = ConversationConverter.convertMessageToDAO(message);
        dao.messages.push(messageDAO);
        await this.update(dao);
    }
    
}
