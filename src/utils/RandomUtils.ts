import { TimeHelper } from "./TimeHelper";
import CryptoJS from "crypto-js";

export class RandomUtils {

    public static getRandomToken(): string {
        return CryptoJS.SHA256(TimeHelper.getNanoTime().toString()).toString();
    }
}