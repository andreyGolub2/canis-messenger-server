export class TimeHelper {

    private constructor() { }

    /**
     * Returns current time in milliseconds.
     */
    public static currentTime(): number {
        return new Date().getTime();
    }

    public static getRemainingTime(endTs: number): number {
        if (endTs == undefined || endTs == null) {
            return null;
        }
        const millis = endTs - TimeHelper.currentTime();
        return millis > 0 ? millis : 0;
    }

    public static getNanoTime(): number {
        const hrTime = process.hrtime();
        return hrTime[0] * 1000000000 + hrTime[1];
    }

}
