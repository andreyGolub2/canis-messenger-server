import { Category } from "../../model/conversation/Category";
import { CategoryDAO } from "../../model/dao/CategoryDAO";

export class CategoryConverter {

    public static convertCategoryToDAO(category: Category): CategoryDAO {
        return {
            _id: category.id,
            name: category.name,
        };
    }

    public static convertDAOtoCategory(dao: CategoryDAO): Category {
        return {
            id: dao._id,
            name: dao.name,
        };
    }
}