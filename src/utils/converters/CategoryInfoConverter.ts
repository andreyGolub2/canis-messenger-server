import { CategoryInfo } from "../../model/conversation/CategoryInfo";
import { Category } from "../../model/conversation/Category";
import { Conversation } from "../../model/conversation/Conversation";
import { ConversationInfoConverter } from "./ConversationInfoConverter";

export class CategoryInfoConverter {

    public static converCategoryToInfo(category: Category, conversation: Conversation[]): CategoryInfo {
        return {
            id: category.id,
            name: category.name,
            conversations: conversation ? conversation.map(conv => {
                return ConversationInfoConverter.convertConversationToInfo(conv);
            }) : null,
        };
    }
}