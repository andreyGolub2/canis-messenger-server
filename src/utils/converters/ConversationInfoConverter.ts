import { Conversation } from "../../model/conversation/Conversation";
import { ConversationInfo } from "../../model/conversation/ConversationInfo";
import { Message } from "../../model/conversation/Message";
import { MessageInfo } from "../../model/conversation/MessageInfo";

export class ConversationInfoConverter {

    public static convertConversationToInfo(conv: Conversation): ConversationInfo {
        return {
            id: conv._id,
            name: conv.name,
            categoryId: conv.categoryId,
            messages: conv.messages ? conv.messages.map(message => {
                return this.convertMessageToInfo(message);
            }) : null,
        };
    }

    public static convertMessageToInfo(message: Message): MessageInfo {
        return {
            id: message.id,
            authorName: message.authorName,
            text: message.text,
            timestamp: message.timestamp,
            convId: message.convId,
        };
    }
}