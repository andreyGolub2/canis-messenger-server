import { User } from "../../model/user/User";
import { UserDAO } from "../../model/dao/UserDAO";
import { Long } from "mongodb";
import { userInfo } from "os";

export class UserConverter {

    public static convertUserToDAO(user: User): UserDAO {
        return {
            _id: Long.fromNumber(user._id),
            name: user.name,
            token: user.token,
            status: Long.fromNumber(user.status),
        };
    }

    public static convertDAOToUser(dao: UserDAO): User {
        return {
            _id: Number(dao._id),
            name: dao.name,
            token: dao.token,
            status: Number(dao.status),
        };
    }
}