import { User } from "../../model/user/User";
import { UserInfo } from "../../model/user/UserInfo";

export class UserInfoConverter {

    public static convertUserToInfo(user: User): UserInfo {
        return {
            id: user._id,
            name: user.name,
            token: user.token
        };
    }
}