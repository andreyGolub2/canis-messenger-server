import { Conversation } from "../../model/conversation/Conversation";
import { ConversationDAO } from "../../model/dao/ConversationDAO";
import { Long } from "mongodb";
import { Message } from "../../model/conversation/Message";
import { MessageDAO } from "../../model/dao/MessageDAO";

export class ConversationConverter {

    public static convertConvToDAO(conv: Conversation): ConversationDAO {
        return {
            _id: conv._id,
            name: conv.name,
            categoryId: conv.categoryId,
            messages: conv.messages ? conv.messages.map(message => {
                return this.convertMessageToDAO(message);
            }) : null,
        };
    }

    public static convertDAOToConv(dao: ConversationDAO): Conversation {
        return {
            _id: dao._id,
            name: dao.name,
            categoryId: dao.categoryId,
            messages: dao.messages ? dao.messages.map(message => {
                return this.convertDAOToMessage(message);
            }) : null,
        };
    }

    public static convertMessageToDAO(message: Message): MessageDAO {
        return {
            _id: message.id,
            authorName: message.authorName,
            authorId: Long.fromNumber(message.authorId),
            text: message.text,
            timestamp: Long.fromNumber(message.timestamp),
            convId: message.convId,
        };
    }

    public static convertDAOToMessage(dao: MessageDAO): Message {
        return {
            id: dao._id,
            authorName: dao.authorName,
            authorId: Number(dao.authorId),
            text: dao.text,
            timestamp: Number(dao.timestamp),
            convId: dao.convId,
        }; 
    }
}