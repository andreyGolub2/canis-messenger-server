export class SocketMessage {
    public static readonly CONNECTION: string = "connection";
    public static readonly DISCONNECT: string = "disconnect";
    public static readonly ADD_NEW_MESSAGE: string = "addNewMessage";
    public static readonly RECEIVE_NEW_MESSAGE: string = "receiveNewMessage";
}