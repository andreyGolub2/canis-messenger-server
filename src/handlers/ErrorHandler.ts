import { Request, Response } from "express";
import { ServerError } from "../errors/ServerError";
import { ServerCodes } from "../enums/ServerCodes";
import { RestResponse } from "../model/common/RestResponse";

const STATUS_OK = 200;
const IS_PRODUCTION = process.env.NODE_ENV == "production";

export class ErrorHandler {
    
    public handle(err: Error, req: Request, res: Response): void {
        res.status(STATUS_OK);
        let code: number, message: string;
        message = err.message;
        code = ServerCodes.SERVER_ERROR;
        if (err instanceof ServerError) {
            code = err.code;
        }

        res.json(RestResponse.fail(code, IS_PRODUCTION ? message : null));
        console.log(`Game server error:
            type = ${err.constructor.name},
            code = ${code},
            message = ${message},
            stack = \n${err.stack}
        `);
    }

}