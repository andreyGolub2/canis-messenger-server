import { Application } from "express";
import express, { Request, Response, NextFunction } from "express";
import { AddressInfo } from "net";
import http from "http";
import * as libs from "./libs";
import * as routes from "./routes";
import cors from "cors";
import * as proxies from "./model/proxy";
import io from "socket.io";

const DEFAULT_PORT: number = 1488;

class Server {

    public app: Application;
    private httpServer: http.Server;
    private shutdownProcessing: boolean;

    private constructor() {
        this.app = express();
        this.initialize();
    }

    public static bootstrap(): Server {
        return new Server();
    }

    private async initialize() {
        this.initCommonMiddleware();
        await this.initModules();
        this.initRouters();
        this.initSockets();
        this.startServer();

        process.on("SIGTERM", this.onShutdown.bind(this));
        process.on("SIGINT", this.onShutdown.bind(this));
    }

    private startServer(): void {
        this.httpServer = this.app.listen(process.env.PORT || DEFAULT_PORT, () => {
            console.log(`Listening on port ${(this.httpServer.address() as AddressInfo).port}`);
        });
    }

    private async initModules(): Promise<void> {
        await libs.init();
    }

    private initSockets() {
        const socket = io();
        proxies.init(socket);
    }

    private initCommonMiddleware(): void {
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(cors());
    }

    private initRouters(): void {
        const apiRouter = express.Router();
        this.app.use("/", apiRouter);
        routes.init(apiRouter);
    }

    private async onShutdown(): Promise<void> {
        if (this.shutdownProcessing) {
            return;
        } else {
            this.shutdownProcessing = true;
        }
        console.log("Process server shutdown...");
        process.exit(0);
    }
}

export const app = Server.bootstrap().app;