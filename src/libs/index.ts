import { MongoProxy } from "./MongoProxy";

let _mongoProxy: MongoProxy;

export const mongoProxy = (): MongoProxy => {
    if (!_mongoProxy) {
        _mongoProxy = new MongoProxy(
            "localhost",
            "root",
            "dev",
            "canis",
            "canis"
        );
    }
    return _mongoProxy;
};

export const init = async (): Promise<void> => {
    await mongoProxy().connect();
};