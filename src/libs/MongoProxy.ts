import mongodb, { Db, MongoClientOptions } from "mongodb";

export class MongoProxy {

    private readonly state = {
        db: <Db>null
    };

    private client: mongodb.MongoClient;

    public constructor(
        private host: string,
        private user: string,
        private password: string,
        private authSource: string,
        private dbName: string,
    ) { }

    public async connect(): Promise<MongoProxy> {
        const url = `mongodb://${this.host}/${this.dbName}?authMechanism=DEFAULT&authSource=${this.authSource}`;
        this.client = await mongodb.MongoClient.connect(url, this.getOptions());
        this.state.db = this.client.db(this.dbName);
        if (this.state.db) {
            console.log("Connected successfully to mongodb server");
        }
        return this;
    }

    public disconnet(): void {
        if (this.client) {
            this.client.close();
        }
    }

    public getState(): { db: Db } {
        return this.state;
    }

    private getOptions(): MongoClientOptions {
        return {
            useNewUrlParser: true,
            auth: {
                user: this.user,
                password: this.password
            }
        };
    }

}
