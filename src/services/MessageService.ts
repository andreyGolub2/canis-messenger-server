import { MongoConversationRepository } from "../repositories/MongoConversationRepository";
import { Conversation } from "../model/conversation/Conversation";
import { ConversationDAO } from "../model/dao/ConversationDAO";
import { ConversationConverter } from "../utils/converters/ConversationConverter";
import { Message } from "../model/conversation/Message";
import { RandomUtils } from "../utils/RandomUtils";
import { CategoryInfo } from "../model/conversation/CategoryInfo";
import { MongoCategoryRepository } from "../repositories/MongoCategoryRepository";
import { CategoryDAO } from "../model/dao/CategoryDAO";
import { Category } from "../model/conversation/Category";
import { CategoryConverter } from "../utils/converters/CategoryConverter";
import { ServerCodes } from "../enums/ServerCodes";
import { ServerError } from "../errors/ServerError";

export class MessageService {

    public constructor(
        private conversationRepository: MongoConversationRepository,
        private categoriesRepository: MongoCategoryRepository,
    ) { }

    public async getAllCategories(): Promise<Category[]> {
        const categoriesDAO: CategoryDAO[] = await this.categoriesRepository.getAll();
        return categoriesDAO.map(dao => {
            return CategoryConverter.convertDAOtoCategory(dao);
        });
    }

    public async getAllConversations(): Promise<Conversation[]> {
        const conversationsDAO: ConversationDAO[] = await this.conversationRepository.getAll();
        return conversationsDAO.map(dao => {
            return ConversationConverter.convertDAOToConv(dao);
        });
    }

    public async getConversation(id: string): Promise<Conversation> {
        const dao: ConversationDAO = await this.conversationRepository.get(id);
        return ConversationConverter.convertDAOToConv(dao);
    }

    public async addMessage(id: string, message: Message): Promise<void> {
        const conv: Conversation = await this.getConversation(id);
        conv.messages.push(message);
        await this.conversationRepository.update(ConversationConverter.convertConvToDAO(conv));
    }

    public async createConversation(name: string, category: string): Promise<void> {
        const dao: CategoryDAO = await this.categoriesRepository.getByName(category);
        if (!dao) {
            throw new ServerError("Invalid category", ServerCodes.SERVER_ERROR);
        }
        const conv: Conversation = {
            _id: RandomUtils.getRandomToken(),
            name: name,
            categoryId: dao._id,
            messages: [],
        };
        this.conversationRepository.insert(ConversationConverter.convertConvToDAO(conv));
    }

    public async createCategory(name: string): Promise<void> {
        const dao: CategoryDAO = await this.categoriesRepository.getByName(name);
        if (dao != null) {
            throw new ServerError("Category already exists", ServerCodes.SERVER_ERROR);
        }
        const category: Category = {
            id: RandomUtils.getRandomToken(),
            name: name,
        };
        await this.categoriesRepository.insert(CategoryConverter.convertCategoryToDAO(category));
    }
}