import { MessageService } from "./MessageService";
import * as repositories from "../repositories";
import { UserService } from "./UserService";

let _messageService: MessageService,
    _userService: UserService;

export const messageService = () => {
    if (!_messageService) {
        _messageService = new MessageService(
            repositories.conversationRepository(),
            repositories.categoryRepository(),
        );
    }
    return _messageService;
};

export const userService = () => {
    if (!_userService) {
        _userService = new UserService(repositories.userRepository());
    }
    return _userService;
};