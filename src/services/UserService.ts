import { MongoUserRepository } from "../repositories/MongoUserRepository";
import { User } from "../model/user/User";
import { TimeHelper } from "../utils/TimeHelper";
import { AccountStatus } from "../enums/AccountStatus";
import { UserConverter } from "../utils/converters/UserConverter";
import { UserDAO } from "../model/dao/UserDAO";
import { RandomUtils } from "../utils/RandomUtils";

export class UserService {

    public constructor(
        private readonly repository: MongoUserRepository,
    ) { }

    public async createUser(): Promise<User> {
        const id: number = Math.floor(TimeHelper.currentTime() / 1000);
        let user = new User();
        user.status = AccountStatus.ACTIVE,
        user.token = RandomUtils.getRandomToken();
        user._id = id,
        user.name = "AnonRetard" + id;
        await this.repository.insert(UserConverter.convertUserToDAO(user));

        return user;
    }

    public async getUser(token: string): Promise<User> {
        const dao: UserDAO = await this.repository.getUserByToken(token);
        return UserConverter.convertDAOToUser(dao);
    }
}