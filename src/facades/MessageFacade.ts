import { MessageService } from "../services/MessageService";
import { Request, Response, NextFunction, response } from "express";
import { RestResponse } from "../model/common/RestResponse";
import { ServerError } from "../errors/ServerError";
import { ServerCodes } from "../enums/ServerCodes";
import { CategoryInfo } from "../model/conversation/CategoryInfo";
import { Category } from "../model/conversation/Category";
import { Conversation } from "../model/conversation/Conversation";
import { CategoryInfoConverter } from "../utils/converters/CategoryInfoConverter";

export class MessageFacade {

    public constructor(
        private messageService: MessageService
    ) { }

    public async getConversations(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            console.log("Conversation requested");
            const categories: Category[] = await this.messageService.getAllCategories();
            const conversations: Conversation[] = await this.messageService.getAllConversations();
            let conversationByCategory: Conversation[] = [];
            const categoryInfos: CategoryInfo[] = categories.map(category => {
                conversationByCategory.length = 0;
                conversationByCategory = conversations.filter(conv => {
                    return conv.categoryId == category.id;
                });
                return CategoryInfoConverter.converCategoryToInfo(category, conversationByCategory);
            });

            res.json(RestResponse.ok(categoryInfos));
            next();
        } catch (err) {
            next(err);
        }
    }

    public async createConversation(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const name: string = req.body.name;
            const category: string = req.body.category ? req.body.category : "";
            if (name == null) {
                throw new ServerError("Name cannot be null", ServerCodes.SERVER_ERROR);
            }
            if (category == null) {
                throw new ServerError("Category cannot be null", ServerCodes.SERVER_ERROR);
            }
            await this.messageService.createConversation(name, category);
            res.json(RestResponse.ok());
        } catch (err) {
            next(err);
        }
    }

    public async createCategory(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const name: string = req.body.name;
            if (name == null) {
                throw new ServerError("Name cannot be null", ServerCodes.SERVER_ERROR);
            }
            await this.messageService.createCategory(name);
            res.json(RestResponse.ok());
        } catch (err) {
            next(err);
        }
    }

    public async getMessagesByConv(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const convId: string = req.body.conversationId;
            if (convId == null) {
                throw new ServerError("Name cannot be null", ServerCodes.SERVER_ERROR);
            }
            const conversation: Conversation = await this.messageService.getConversation(convId);
            res.json(RestResponse.ok(conversation.messages));
        } catch (err) {
            next(err);
        }
    }
}