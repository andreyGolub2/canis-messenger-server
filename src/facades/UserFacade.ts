import { Request, Response, NextFunction } from "express";
import { RestResponse } from "../model/common/RestResponse";
import { UserService } from "../services/UserService";
import { User } from "../model/user/User";
import { UserInfoConverter } from "../utils/converters/UserInfoConverter";

export class UserFacade {

    public constructor(
        private readonly userService: UserService,
    ) { }

    public async login(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const token: string = req.body.token;
            let user: User;
            if (token != undefined) {
                user = await this.userService.getUser(token);
                console.log(`Logged in user, id:${user._id}, name:${user.name}`);
            } else {
                user = await this.userService.createUser();
                console.log(`Created new user, id:${user._id}, name:${user.name}`);
            }
            res.json(RestResponse.ok(UserInfoConverter.convertUserToInfo(user)));
            next();
        } catch (err) {
            next(err);
        }
    }

}