import { MessageFacade } from "./MessageFacade";
import * as services from "../services";
import { UserFacade } from "./UserFacade";

let _messageFacade: MessageFacade,
    _userFacade: UserFacade;

export const messageFacade = () => {
    if (!_messageFacade) {
        _messageFacade = new MessageFacade(services.messageService());
    }
    return _messageFacade;
};

export const userFacade = () => {
    if (!_userFacade) {
        _userFacade = new UserFacade(services.userService());
    }

    return _userFacade;
};